﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperLibrary.Cmd
{
    public sealed class Department : ICountingBook, IComparable<Department>
    {
        public String DepartmentName { get; set; }
        public List<Book> Books { get; set; }
        public Department()
        {
            Books = new List<Book>();
        }
        public Department(String departName) : this()
        {
            DepartmentName = departName;
        }
        public Department(String departName, IEnumerable<Book> books) : this(departName)
        {

            foreach (var b in books)
            {
                this.Books.Add(b);
            }
        }
        public Int32 GetCountBook()
        {
            Int32 countBookInDepartment = this.Books.Count;
            return countBookInDepartment;
        }
        public Int32 CompareTo(Department other)
        {
            if (ReferenceEquals(this, other))
            {
                return 0;
            }
            if (ReferenceEquals(null, other))
            {
                return 1;
            }
            Int32 countBooksInThisDepartment = this.GetCountBook();
            Int32 countBooksInOtherDepartment = other.GetCountBook();

            Int32 comparedepart = countBooksInThisDepartment.CompareTo(countBooksInOtherDepartment);
            return comparedepart;
        }
        public override string ToString()
        {
            return DepartmentName;
        }
    }
}
