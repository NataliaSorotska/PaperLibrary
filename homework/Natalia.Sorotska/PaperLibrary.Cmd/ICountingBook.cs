﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperLibrary.Cmd
{
    interface ICountingBook
    {
        int GetCountBook();
    }
}
