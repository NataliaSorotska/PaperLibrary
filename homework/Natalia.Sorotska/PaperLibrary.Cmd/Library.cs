﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperLibrary.Cmd
{
    public sealed class Library
    {
        public string LibraryName { get; set; }

        private List<Department> departments;
        private List<Author> authors;
        public Library()
        {
            departments = new List<Department>();
            authors = new List<Author>();
        }
        public Library(string libraryName) : this()
        {
            this.LibraryName = libraryName;
        }
        public void SetAuthorInLibrary(Author author)
        {
            this.authors.Add(author);
        }
        public void SetDepartmentInLibrary(Department department)
        {
            this.departments.Add(department);
        }
        public override string ToString()
        {
            return LibraryName;
        }
        public Author GetAuthorWithMaxBoksCount()
        {
            Int32 maxBooksCount = authors.Max(x => x.Books.Count);
            Author authorWithMaxBooksCount = authors.FirstOrDefault(x => x.Books.Count == maxBooksCount);
            return authorWithMaxBooksCount;
        }
        public Department GetDepartmentWithMaksBookCount()
        {
            Int32 maxBookCount = departments.Max(x => x.Books.Count);
            Department departmentWithMaxBookCount = departments.FirstOrDefault(x => x.Books.Count == maxBookCount);
            return departmentWithMaxBookCount;
        }
        public Book GetBookWithMinPageCount()
        {
            IEnumerable<Book> allBooks = departments.SelectMany(x => x.Books);
            UInt32 minPageCount = allBooks.Min(x => x.PageCount);
            Book bookWithMinPageCount = allBooks.FirstOrDefault(x => x.PageCount == minPageCount);
            return bookWithMinPageCount;
        }
    }
}
