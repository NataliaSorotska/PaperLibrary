﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperLibrary.Cmd
{
    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library();

            library.LibraryName = "Central Library";


            Department department1 = new Department("Science");
            Department department2 = new Department("Fantasy");

            Author author1 = new Author("Richard", "Dawkinz");
            Author author2 = new Author("Stephen", "King");

            Book book1 = new Book("Blaind Watchmaker ", author1, 883);
            Book book2 = new Book("The Selfish Gene", author1, 973);
            Book book3 = new Book("The God Delusion", author1, 737);
            Book book4 = new Book("The Ancestor's Tale", author1, 787);

            Book book5 = new Book("Fog", author2, 356);
            Book book6 = new Book("Dream Catcher", author2, 435);
            Book book7 = new Book("Cell", author2, 323);

            author1.Books.Add(book1);
            author1.Books.Add(book2);
            author1.Books.Add(book3);
            author1.Books.Add(book4);

            author2.Books.Add(book5);
            author2.Books.Add(book6);
            author2.Books.Add(book7);


            department1.Books.Add(book1);
            department1.Books.Add(book2);
            department1.Books.Add(book3);
            department1.Books.Add(book4);

            department2.Books.Add(book5);
            department2.Books.Add(book6);
            department2.Books.Add(book7);


            library.SetAuthorInLibrary(author1);
            library.SetAuthorInLibrary(author2);

            library.SetDepartmentInLibrary(department1);
            library.SetDepartmentInLibrary(department2);


            Console.WriteLine($"Hi you are in library: {library.LibraryName}");

            Author authorWithMaxBooksCount = library.GetAuthorWithMaxBoksCount();
            Console.WriteLine($"Author with max books count  - {authorWithMaxBooksCount}");

            Department departmentWithMaxBooksCount = library.GetDepartmentWithMaksBookCount();
            Console.WriteLine($"Department with max books count - {departmentWithMaxBooksCount}");

            Book bookWithMinPageCountInLibrary = library.GetBookWithMinPageCount();
            Console.WriteLine($"Book with min page count in library - {bookWithMinPageCountInLibrary}");
            Console.ReadLine();
        }
    }
}

